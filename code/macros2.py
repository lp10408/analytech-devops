import pandas as pd
import numpy as np


def zsmeans(df, title=''):
    """\n
    ######################################################################################
    ##                           USAGE OF ZSMEANS FUNCTION                              ##
    ##                                                                                  ##
    ##   ZSMEANS prints in the list file summary statistics for a dataset               ##
    ##   ZSMEANS generates the following statistics for all numeric field in            ##
    ##   the dataset: number of observations, sum, maximum, minimum,                    ##
    ##   mean,std,25%,50%,75%                                                           ##
    ##   Usage:                                                                         ##
    ##   > from Macros import zsmeans                                                   ##
    ##   > zsmeans(df,title='')                                                         ##
    ##   ** where df is input dataframe or series, and title is the user defined        ##
    ##      title for the output (default value "ZS Means Output")                      ##
    ######################################################################################
	\n
    """
    try:
        if title == '':
            print('ZS Means output')
        else:
            print(title)
        return df.describe().unstack().unstack()
    except AttributeError:
        print("Error Occured :  incorrect input type. This function only works for Dataframe or Series")
        print(zsmeans.__doc__)
        return "Error"
    except Exception as e:
        print(e)
        print(zsmeans.__doc__)
        return "Error"


def zsmeans2(df, *args, title=''):
    """\n
    ######################################################################################
    ##                           USAGE OF ZSMEANS2 FUNCTION                             ##
    ##                                                                                  ##
    ##   ZSMEANS prints in the list file summary statistics for a dataset               ##
    ##   ZSMEANS generates the following statistics for given numeric field in          ##
    ##   the dataset: number of observations, sum, maximum, minimum,                    ##
    ##   mean,std,25%,50%,75%                                                           ##
    ##   Usage:                                                                         ##
    ##   > from Macros import zsmeans2                                                  ##
    ##   > zsmeans2(df,*args,title='')                                                  ##
    ##   ** where df is input dataframe or series, * args (optional) are the columns    ##
    ##      to be displayed and title is the user defined title for the output          ##
    ##      (default value "ZS Means Output")                                           ##
    ##                                                                                  ##
    ######################################################################################
    \n
    """
    try:
        if isinstance(df, pd.DataFrame):
            if title == '':
                print('ZS Means output')
            else:
                print(title)
            if args == ():
                return df.describe().unstack().unstack()
            else:
                return df[list(args)].describe().unstack().unstack()
        else:
            print("Invalid Argument - First Argument must be a pandas dataframe")
            raise AttributeError
    except AttributeError:
        print("Error Occured :  incorrect input type. This function only works for Dataframe or Series")
        print(zsmeans2.__doc__)
        return "Error"
    except KeyError:
        print('Error : argument(s) not present in dataframe/series')
        print(zsmeans2.__doc__)
        return "Error"
    except Exception as e:
        print(e)
        print(zsmeans2.__doc__)
        return "Error";


def zsnobs(df):
    """\n
    ######################################################################################
    ##                           USAGE OF ZSNOBS FUNCTION                               ##
    ##                                                                                  ##
    ##   Prints number of obsevations in a dataset or series                            ##
    ##   Usage:                                                                         ##
    ##   > from Macros import zsnobs                                                    ##
    ##   > zsnobs(df)                                                                   ##
    ##   ** where df is input dataframe or series,                                      ##
    ##                                                                                  ##
    ######################################################################################
    \n
    """
    try:
        return df.shape[0]
    except AttributeError:
        print("Error Occured :  incorrect input type. This function only works for Dataframe or Series")
        print(zsnobs.__doc__)
        return "Error"
    except Exception as e:
        print(e)
        print(zsnobs.__doc__)
        return "Error";


def zsncol(df):
    """\n
    ######################################################################################
    ##                           USAGE OF ZSNCOL FUNCTION                               ##
    ##                                                                                  ##
    ##   Prints number of columns in a dataset or series                                ##
    ##   Usage:                                                                         ##
    ##   > from Macros import zscols                                                    ##
    ##   > zsncol(df)                                                                   ##
    ##   ** where df is input dataframe or series,                                      ##
    ##                                                                                  ##
    ######################################################################################
    \n
    """
    try:
        return df.shape[1]
    except AttributeError:
        print("Error Occured :  incorrect input type. This function only works for Dataframe or Series")
        print(zsncol.__doc__)
        return "Error"
    except Exception as e:
        print(e)
        print(zsncol.__doc__)
        return "Error";


def zssamp(df, *args, n=5):
    """\n
    ######################################################################################
    ##                           USAGE OF ZSSAMP FUNCTION                               ##
    ##                                                                                  ##
    ##   Return first, random and last n records of dataset,                            ##
    ##   also print the no of records present in the dataset                            ##
    ##   Usage:                                                                         ##
    ##   > from Macros import zssamp                                                    ##
    ##   > zssamp(df,*args,n=5)                                                         ##
    ##   ** where df is input dataframe or series, *args (optional) is/are the          ##
    ##      column(s) for which sample is needed, and n (optional; default 5) is the    ##
    ##      number of observations needed                                               ##
    ##   ** Note: args must be passed in inverted commas. Eg: 'col1', 'col2'            ##                                                ##
    ##                                                                                  ##
    ######################################################################################
    \n
    """
    try:
        if isinstance(df, pd.DataFrame):
            if zsnobs(df) < n:
                n = zsnobs(df)
                print(f'No of records in the dataset is/are: {zsnobs(df)}')
                if args == ():
                    df1 = df.head(n)
                    df1.insert(0, '__Test__', 'First')
                    df2 = df.sample(n)
                    df2.insert(0, '__Test__', 'Random')
                    df3 = df.tail(n)
                    df3.insert(0, '__Test__', 'Last')
                    return df1.append(df2).append(df3).set_index('__Test__')
                else:
                    df1 = df[list(args)].head(n)
                    df1.insert(0, '__Test__', 'First')
                    df2 = df[list(args)].sample(n)
                    df2.insert(0, '__Test__', 'Random')
                    df3 = df[list(args)].tail(n)
                    df3.insert(0, '__Test__', 'Last')
                    return df1.append(df2).append(df3).set_index('__Test__')
            else:
                print(f'No of records in the dataset is/are: {zsnobs(df)}')
                if args == ():
                    df1 = df.head(n)
                    df1.insert(0, '__Test__', 'First')
                    df2 = df.sample(n)
                    df2.insert(0, '__Test__', 'Random')
                    df3 = df.tail(n)
                    df3.insert(0, '__Test__', 'Last')
                    return df1.append(df2).append(df3).set_index('__Test__')
                else:
                    df1 = df[list(args)].head(n)
                    df1.insert(0, '__Test__', 'First')
                    df2 = df[list(args)].sample(n)
                    df2.insert(0, '__Test__', 'Random')
                    df3 = df[list(args)].tail(n)
                    df3.insert(0, '__Test__', 'Last')
                    return df1.append(df2).append(df3).set_index('__Test__')
        else:
            print("Invalid Argument - First Argument must be a pandas dataframe")
            raise AttributeError
    except AttributeError:
        print('Error : input must be a dataframe or series')
        print(zssamp.__doc__)
        return "Error"
    except KeyError:
        print('Error : argument(s) not present in dataframe/series')
        print(zssamp.__doc__)
        return "Error"
    except Exception as e:
        print(e)
        print(zssamp.__doc__)
        return "Error";


def zslist(df, *args):
    """\n
    ######################################################################################
    ##                         USAGE OF ZSLIST FUNCTION                                 ##
    ##                                                                                  ##
    ##   Return unique combinations of multiple fields and provides count of each       ##
    ##   Syntax:                                                                        ##
    ##   > from Macros import zslist                                                    ##
    ##   > zslist(df,*args)                                                             ##
    ##   ** where df is input dataframe and *args (optional) are column(s)              ##
    ##                                                                                  ##
    ######################################################################################
    \n
    """
    try:
        if isinstance(df, pd.DataFrame):
            if args == ():
                try:
                    df1 = df.fillna('_Null_')

                    return df1.groupby(list(df1.columns)).size().reset_index().rename(columns={0: '__List_Count__'})
                except:

                    return df.groupby(list(df.columns)).size().reset_index().rename(columns={0: '__List_Count__'})
            else:
                try:
                    df1 = df.fillna('_Null_')

                    return df1.groupby(list(args)).size().reset_index().rename(columns={0: '__List_Count__'})
                except:

                    return df.groupby(list(args)).size().reset_index().rename(columns={0: '__List_Count__'})
        else:
            print("Invalid Argument - First Argument must be a pandas dataframe")
            raise TypeError
    except TypeError:
        print("Error Occured")
        print(zslist.__doc__)
        return "Error"
    except Exception as e:
        print(e)
        print(zslist.__doc__)
        return "Error";


def zslvlchk(df, *args):
    """\n
    ######################################################################################
    ##                           USAGE OF ZSLVLCHK FUNCTION                             ##
    ##                                                                                  ##
    ##   Checks the uniqueness of data and print out duplicates if any                  ##
    ##   Usage:                                                                         ##
    ##   > from Macros import zslvlchk                                                  ##
    ##   > zslvlchk(df,*args)                                                           ##
    ##   ** where df is input dataframe and *args (optional) are columns(s)             ##
    ##                                                                                  ##
    ######################################################################################
    \n
    """
    try:
        if isinstance(df, pd.DataFrame):
            if args == ():
                df1 = df[df.duplicated(list(df.columns), keep=False)]
                if zsnobs(df1) == 0:
                    print(' Dataset is unique')
                else:
                    print(f'{zsnobs(df1)} Duplicate records in dataset at this level ')
                    return df1
            else:

                df1 = df[df.duplicated(list(args), keep=False)]
                if zsnobs(df1) == 0:
                    if len(args) == 1:
                        print('Data is unique at ' + str(args)[1:-2] + ' level')
                    else:
                        print('Data is unique at ' + str(args)[1:-1] + ' level')
                else:
                    if len(args) == 1:
                        print(f'{zsnobs(df1)} Duplicates found at {str(args)[1:-2]} level')
                        return df1
                    else:
                        print(f'{zsnobs(df1)} Duplicates found at {str(args)[1:-1]} level')
                        return df1
        else:
            print("Invalid Argument - First Argument must be a pandas dataframe")
            raise TypeError
    except TypeError:
        print("Error Occured")
        print(zslvlchk.__doc__)
        return "Error"
    except Exception as e:
        print(e)
        print(zslvlchk.__doc__)
        return "Error";


def zsrandom(df, *args, n=5):
    """\n
    ######################################################################################
    ##                           USAGE OF ZSRANDOM FUNCTION                             ##
    ##                                                                                  ##
    ##   Prints Random n observations from the dataset                                  ##
    ##   Usage:                                                                         ##
    ##   > from Macros import zsrandom                                                  ##
    ##   > zsrandom(df,*args,n=10)                                                      ##
    ##   ** where df is input dataframe or series, *args (optional) are columns(s)      ##
    ##      and n is the number of records (optional; by default set to 5)              ##
    ##   ** Note: args must be passed in inverted commas. Eg: 'col1', 'col2'            ##
    ##                                                                                  ##
    ######################################################################################
    \n
    """
    try:
        if zsnobs(df) < n:
            n = zsnobs(df)
            if args == ():
                return df.sample(n)
            else:
                return df[list(args)].sample(n)
        else:
            if args == ():
                return df.sample(n)
            else:
                return df[list(args)].sample(n)
    except AttributeError:
        print('input must be a dataframe or series')
        print(zsrandom.__doc__)
        return "Error"
    except KeyError:
        print('argument(s) not present in dataframe/series')
        print(zsrandom.__doc__)
        return "Error"
    except Exception as e:
        print(e)
        print(zsrandom.__doc__)
        return "Error";


def zssrtsum(df, grp_list, value_list, freq=0, alphas=0, total=0):
    """\n
    ######################################################################################
    ##                           USAGE OF ZSSRTSUM FUNCTION                             ##
    ##                                                                                  ##
    ##   Aggregates value fields according to specified column groups.                  ##
    ##   Also prints the frequency first value and subtotal at group level              ##
    ##   Usage:                                                                         ##
    ##   > from Macros import zssrtsum                                                  ##
    ##   > zssrtsum(df,grp_list,value_list,freq = 0,alphas = 0,total = 0)               ##
    ##   ** where df is input dataframe or series, grp_list are columns(s) to group by  ##
    ##      and value_list are column(s) whose values need to be rolled up              ##
    ##      freq = 0 or 1 to toggle group level frequency, alphas = 0 or 1 to toggle    ##
    ##      group level first records, and total = 0 or 1 to toggle group subtotals     ##
    ##   ** Note: columns must be passed in inverted commas. Eg: 'col1', 'col2'         ##
    ##   ** Note: grp_list and value_list should be enclosed in []                      ##
    ##   ** Note: in case grp_list or value_list have multiple columns, they should be  ##
    ##      comma-separated. Eg: zssrtsum(df, ['col1','col2'],['val1','val2'])          ##
    ##                                                                                  ##
    ######################################################################################
    \n
    """
    try:
        if isinstance(df, pd.DataFrame):
            for j in range(len(value_list)):
                if df[value_list[j]].dtype != 'O':
                    i = 2
                else:
                    i = 0
                    return (f'{value_list[j]} column is not numeric type')

            df1 = df.groupby(grp_list)[value_list].sum().reset_index()
            if freq == 1:
                df1['__Freq__'] = df.groupby(list(grp_list)).size().reset_index().rename(columns={0: '__List_Count__'})[
                    '__List_Count__']
            if total == 1:
                if freq == 1:
                    df1.loc['Total'] = df1[list(value_list) + list(['__Freq__'])].sum()
                    df1.insert(len(df1.columns) - 1, '__Total__', df1[list(value_list)].sum(axis=1))
                else:
                    df1.loc['Total'] = df1[list(value_list)].sum()
                    df1.insert(len(df1.columns), '__Total__', df1[list(value_list)].sum(axis=1))
            if alphas == 1:
                df2 = df.groupby(grp_list)[value_list].nth(0).reset_index()[value_list].rename(
                    columns=lambda columns: f'_First_{str(columns)}')
                df3 = df1.assign(**df2)
                return df3
            else:
                return df1
        else:
            print("Invalid Argument - First Argument must be a pandas dataframe")
            raise AttributeError
    except AttributeError:
        print("Error Occured :  incorrect input type. This function only works for Dataframe")
        print(zssrtsum.__doc__)
        return "Error"
    except KeyError:
        print('Error : argument(s) not present in dataframe')
        print(zssrtsum.__doc__)
        return "Error"
    except Exception as e:
        print(e)
        print(zssrtsum.__doc__)
        return "Error";


def zssort(df, *args, asc=True, nap='last'):
    """\n
    ######################################################################################
    ##                           USAGE OF ZSSORT FUNCTION                               ##
    ##                                                                                  ##
    ##   Sorts the data by the given argument (column(s))                               ##
    ##   Usage:                                                                         ##
    ##   > from Macros import zssort                                                    ##
    ##   > zssort(df,*args)                                                             ##
    ##   ** where df is input dataframe or series and args are the column(s) to sort    ##
    ##                                                                                  ##
    ######################################################################################
    \n
    """
    try:
        if isinstance(df, pd.DataFrame):
            return df.sort_values(by=list(args), axis=0, ascending=asc, na_position=nap)
        else:
            print("Invalid Argument - First Argument must be a pandas dataframe")
            raise TypeError
    except AttributeError:
        print("Error Occured :  incorrect input type. This function only works for Dataframe")
        print(zssort.__doc__)
        return "Error"
    except KeyError:
        print('Error : argument(s) not present in dataframe')
        print(zssort.__doc__)
        return "Error"
    except Exception as e:
        print(e)
        return "Error";


def zssort2(df, *args, asc=True, nap='last'):
    """\n
    ######################################################################################
    ##                           USAGE OF ZSSORT2 FUNCTION                              ##
    ##                                                                                  ##
    ##   Sorts the data by the given argument (column(s)) and return only sorted        ##
    ##   variable (column(s))                                                           ##
    ##   Usage:                                                                         ##
    ##   > from Macros import zssort2                                                   ##
    ##   > zssort2(df,*args)                                                            ##
    ##   ** where df is input dataframe or series and args are the column(s) to sort    ##
    ##                                                                                  ##
    ######################################################################################
    \n
    """
    try:
        if isinstance(df, pd.DataFrame):
            return df[list(args)].sort_values(by=list(args), axis=0, ascending=asc, na_position=nap)
        else:
            print("Invalid Argument - First Argument must be a pandas dataframe")
            raise TypeError
    except AttributeError:
        print("Error Occured :  incorrect input type. This function only works for Dataframe")
        print(zssort2.__doc__)
        return "Error"
    except KeyError:
        print('Error : argument(s) not present in dataframe')
        print(zssort2.__doc__)
        return "Error"
    except Exception as e:
        print(e)
        return "Error";


def zsadjzero(dx, n=0):
    """\n
    ######################################################################################
    ##                           USAGE OF ZSADJZERO FUNCTION                            ##
    ##                                                                                  ##
    ##   Replaces blanks in fields by zeros(by default)                                 ##
    ##   Creates two new columns :                                                      ##
    ##   1. __NullCount__ for each row                                                  ##
    ##   2. __Flag__ = 1 for rows where atleast 1 null value is present                 ##
    ##   Usage:                                                                         ##
    ##   > from Macros import zsadjzero                                                 ##
    ##   > zsadjzero(df,n=0)                                                            ##
    ##   ** where df is input dataframe or series and n (optional; default 0) is the    ##
    ##      value with which nulls are to be replaced                                   ##
    ##                                                                                  ##
    ######################################################################################
    \n
    """
    try:
        if isinstance(dx, pd.DataFrame):
            df = dx.copy(deep=False)
            df['__NullCount__'] = df.isnull().sum(axis=1)
            df['__Flag__'] = [1 if x > 0 else 0 for x in df['__NullCount__']]
            df1 = df.fillna(n)
            return df1
        elif isinstance(dx, pd.Series):
            df = dx.copy(deep=False)
            df1 = df.fillna(n)
            return df1
        else:
            print("Invalid Argument - First Argument must be a pandas dataframe or series")
            raise AttributeError
    except AttributeError:
        print("Error Occured :  incorrect input type. This function only works for Dataframe or Series")
        print(zsadjzero.__doc__)
        return "Error"
    except ValueError:
        print("Some column(s) in the input data are of Categorical datatype")
        print(zsadjzero.__doc__)
        return "Error"
    except Exception as e:
        print(e)
        print(zsadjzero.__doc__)
        return "Error";


def zssay(quoted, unquoted):
    """\n
    ######################################################################################
    ##                           USAGE OF ZSSAY FUNCTION                                ##
    ##                                                                                  ##
    ##   To Write out message to log;                                                   ##
    ##   QUOTED   : fixed string of characters eg TERR NOT FOUND                        ##
    ##   UNQUOTED : variable names, eg ZIP= CITY= ST=                                   ##
    ##   Usage:                                                                         ##
    ##   > from Macros import zssay                                                     ##
    ##   > zssay(quoted,unquoted)                                                       ##
    ##   ** where quoted is a fixed string of characters, and unquoted is any variable  ##
    ##                                                                                  ##
    ######################################################################################
    \n
    """
    try:
        print(f'{quoted} {unquoted}')
    except Exception as e:
        print(e)
        print(zssay.__doc__)
        return "Error";


def zsrankby(dx, rank_var, groups, *args, x=0, group_var='__Bucket__'):
    """\n
    ######################################################################################
    ##                           USAGE OF ZSRANKBY FUNCTION                             ##
    ##                                                                                  ##
    ##   ZSRANKBY classifies a set of observations into ranked groups                   ##
    ##   (e.g., percentiles, deciles, quartiles).                                       ##
    ##   Usage:                                                                         ##
    ##   > from Macros import zsrankby                                                  ##
    ##   > zsrankby(dx,rank_var,groups,*args,x = 0,group_var = '__Bucket__')            ##
    ##   ** where dx is the input dataset, rank_var is the variable to rank on,         ##
    ##   groups is the number of ranks to be created,                                   ##
    ##   *args are the columns to group on (rank inside groups) (optional)              ##
    ##   x is the type of ranking to be done (optional, default value 0)                ##
    ##        -x = 0 implies each group will have approximately equal number of         ##
    ##              observations                                                        ##
    ##          -x = 1 implies each group will contain approximately equal volume       ##
    ##              of ranking variable                                                 ##
    ##          -x = 2 is similar to x=0, but any tied value cases would be put in      ##
    ##              the same group (in the higher group)                                ##
    ##   Note that when ranks are created, lower bucket value signifies higher ranks,   ##
    ##   i.e. rank 1 is the highest and so on                                           ##
    ##                                                                                  ##
    ######################################################################################
    \n
    """
    try:
        if isinstance(dx, pd.DataFrame):
            df = dx.copy(deep=False)
            if args == ():
                if x == 1:
                    df = zssort(df, rank_var, asc=False)
                    df['__CumSum__'] = ((df[rank_var].expanding(1).sum()) / df[rank_var].sum()) * 100
                    df[group_var] = 0
                    for i in range(1, groups + 1):
                        df.loc[(df['__CumSum__'] <= (100 / groups) * i) & (
                                    df['__CumSum__'] > (100 / groups) * (i - 1)), group_var] = i
                    df.pop('__CumSum__')
                    return df
                elif x == 2:
                    df['__Rank__'] = df[rank_var].rank(method='min', ascending=False)
                    df[group_var] = 0
                    j = df.shape[0]
                    for i in range(1, groups + 1):
                        df.loc[(df['__Rank__'] <= (j / groups) * i) & (
                                    df['__Rank__'] > (j / groups) * (i - 1)), group_var] = i
                    df.pop('__Rank__')
                    df = df.sort_values(group_var, ascending=False)
                    return df
                else:
                    df = zssort(df, rank_var, asc=False)
                    df['__dummy_x__'] = 1
                    df['__CumSum__'] = df['__dummy_x__'].expanding(1).sum()
                    df[group_var] = 0
                    j = df.shape[0]
                    for i in range(1, groups + 1):
                        df.loc[(df['__CumSum__'] <= (j / groups) * i) & (
                                    df['__CumSum__'] > (j / groups) * (i - 1)), group_var] = i
                    df.pop('__CumSum__')
                    df.pop('__dummy_x__')
                    return df
            else:

                if x == 1:
                    df = zssort(df, *args, rank_var, asc=False)
                    df['__dummy_x__'] = df.groupby(list(args))[rank_var].cumsum()
                    df['__dummy_y__'] = df.groupby(list(args))['__dummy_x__'].transform(max)
                    df['__CumSum__'] = (df['__dummy_x__'] / df['__dummy_y__']) * 100

                    df.pop('__dummy_x__')
                    df.pop('__dummy_y__')
                    for i in range(1, groups + 1):
                        df.loc[(df['__CumSum__'] <= (100 / groups) * i) & (
                                    df['__CumSum__'] > (100 / groups) * (i - 1)), group_var] = i
                    df.pop('__CumSum__')
                    df = df.astype({group_var: 'int64'})
                    return df
                elif x == 2:
                    # df = zssort(df,*args,rank_var, asc = False)
                    df['__dummy_x__'] = df.groupby(list(args))[rank_var].rank(method='min', ascending=False)
                    df['__dummy_y__'] = df.groupby(list(args))['__dummy_x__'].transform(max)
                    df['__CumSum__'] = (df['__dummy_x__'] / df['__dummy_y__']) * 100

                    df.pop('__dummy_x__')
                    df.pop('__dummy_y__')
                    for i in range(1, groups + 1):
                        df.loc[(df['__CumSum__'] <= (100 / groups) * i) & (
                                    df['__CumSum__'] > (100 / groups) * (i - 1)), group_var] = i
                    df.pop('__CumSum__')
                    df = df.astype({group_var: 'int64'})
                    return df
                else:
                    df = zssort(df, *args, rank_var, asc=False)
                    df['__dummy_x__'] = df.groupby(list(args))[rank_var].cumcount() + 1
                    df['__dummy_y__'] = df.groupby(list(args))['__dummy_x__'].transform(max)
                    df['__CumSum__'] = (df['__dummy_x__'] / df['__dummy_y__']) * 100

                    df.pop('__dummy_x__')
                    df.pop('__dummy_y__')
                    for i in range(1, groups + 1):
                        df.loc[(df['__CumSum__'] <= (100 / groups) * i) & (
                                    df['__CumSum__'] > (100 / groups) * (i - 1)), group_var] = i
                    df.pop('__CumSum__')
                    df = df.astype({group_var: 'int64'})
                    return df
        else:
            print("Invalid Argument - First Argument must be a pandas dataframe")
            raise AttributeError
    except AttributeError:
        print("Error Occured :  incorrect input type. This function only works for Dataframe")
        print(zsrankby.__doc__)
        return "Error"
    except KeyError:
        print('Error : argument(s) not present in dataframe')
        print(zsrankby.__doc__)
        return "Error"
    except Exception as e:
        print(e)
        print(zsrankby.__doc__)
        return "Error";


def zsdofor(x, y, code, rplc='!'):
    """\n
    ######################################################################################
    ##                           USAGE OF ZSDOFOR FUNCTION                              ##
    ##                                                                                  ##
    ##   ZSDOFOR repeatedly generates a fragment of SAS code by iterating over          ##
    ##   a range of numbers specified by the user.                                      ##
    ##                                                                                  ##
    ##   Usage (to generate code):                                                      ##
    ##   > from Macros import zsdofor                                                   ##
    ##   > zsdofor(x,y,code,rplc ='!'))                                                 ##
    ##                                                                                  ##
    ##   Usage (to run iterated code):                                                  ##
    ##   > from Macros import zsdofor                                                   ##
    ##   > exec(compile('\n'.join(zsdofor(x,y,code,rplc ='!')),'<string>','exec'))      ##
    ##                                                                                  ##
    ##   ** where x and y denote the start and end points of the iterations (x <= y).   ##
    ##      code is the code snippet with the symbol to be replaced from x to y,        ##
    ##      and rplc is the symbol to be replaced (optional, default value '!')         ##
    ##                                                                                  ##
    ######################################################################################
    \n
    """
    try:
        if x <= y:
            z = list()
            while x <= y:
                z.append(f'{code.replace(rplc, str(x))}')
                x = x + 1
            return z
        else:
            print('Error: y should not be less than x')
            # raise Exception
    except AttributeError:
        print('This macro does not support input dataset type')
        print(zsdofor.__doc__)
        return "Error"
    except Exception as e:
        print(e)
        print(zsdofor.__doc__)
        return "Error";


def zsdoscan(code, *args, rplc='!'):
    """\n
    ######################################################################################
    ##                           USAGE OF ZSDOSCAN FUNCTION                             ##
    ##                                                                                  ##
    ##   ZSSCAN repeatedly generates a fragment of code, each time substituting a new   ##
    ##   text value into the fragment from a list of values supplied by the user.       ##
    ##                                                                                  ##
    ##   Usage (to generate code):                                                      ##
    ##   > from Macros import zsdoscan                                                  ##
    ##   > zsdoscan(code,*args,rplc ='!'))                                              ##
    ##                                                                                  ##
    ##   Usage (to run iterated code):                                                  ##
    ##   > from Macros import zsdoscan                                                  ##
    ##   > exec(compile('\n'.join(zsdoscan(code,*args,rplc ='!')),'<string>','exec'))   ##
    ##                                                                                  ##
    ##    **where                                                                       ##
    ##        --code is the code snippet with the symbol to be replaced by              ##
    ##            the list of values                                                    ##
    ##        --*args is the list of values to replace in the code snippet              ##
    ##        --rplc is the symbol to be replaced (optional, default value '!')         ##
    ##    *Note that if the list of values passed under args are enclosed in brackets,  ##
    ##     the macro will consider it as a single entity.                               ##
    ##     Eg: ['1','2','3'] as args will replace ! in the code entirely                ##
    ##                                                                                  ##
    ######################################################################################
    \n
    """
    try:
        if args == ():
            raise KeyError
        else:
            i = 0
            z = list()
            while i < len(args):
                z.append(f'{code.replace(rplc, str(list(args)[i]))}')
                i = i + 1
            return z
    except KeyError:
        print('Please enter list of values to iterate the code')
        print(zsdoscan.__doc__)
        return "Error"
    except AttributeError:
        print('This macro does not support input dataset type')
        print(zsdoscan.__doc__)
        return "Error"
    except Exception as e:
        print(e)
        print(zsdoscan.__doc__)
        return "Error";


def zsprnt(df, *args, k=5):
    """\n
    ######################################################################################
    ##                           USAGE OF ZSPRNT FUNCTION                               ##
    ##                                                                                  ##
    ##   Print statistics of numerical columns                                          ##
    ##   Return first, random and last n records of the dataset, by default n = 5       ##
    ##                                                                                  ##
    ##   Usage                                                                          ##
    ##   > from Macros import zsprnt                                                    ##
    ##   > zsprnt(df,*args,k=5)                                                         ##
    ##                                                                                  ##
    ##    **where df is the dataframe, *args are the list of columns to be displayed    ##
    ##     (optional; by default displays all columns), k is the number of records to   ##
    ##      be displayed (optional; by default 5)                                       ##
    ##                                                                                  ##
    ######################################################################################
    \n
    """
    try:
        if args == ():
            print(df.describe().unstack().unstack())
            print('\n')
            return zssamp(df, n=k)
        else:
            print(df[list(args)].describe().unstack().unstack())
            print('\n')
            return zssamp(df, *args, n=k)
    except AttributeError:
        print('This macro does not support input dataset type')
        print(zsprnt.__doc__)
        return "Error"
    except KeyError:
        print('Error : argument(s) not present in the input dataset')
        print(zsprnt.__doc__)
        return "Error"
    except Exception as e:
        print(e)
        print(zsprnt.__doc__)
        return "Error";


def zsprikey(df, *args):
    """\n
    ######################################################################################
    ##                           USAGE OF ZSPRIKEY FUNCTION                             ##
    ##                                                                                  ##
    ##   Checks whether the given columns can be used as a valid primary key for the    ##
    ##   input dataset. If no arguments are entered, it returns all the valid primary   ##
    ##   keys for the dataset                                                           ##
    ##                                                                                  ##
    ##   Usage                                                                          ##
    ##   > from Macros import zsprikey                                                  ##
    ##   > zsprikey(df,*args)                                                           ##
    ##                                                                                  ##
    ##    **where df is the dataframe, *args are the list of columns to be checked      ##
    ##    (optional, if no columns entered, it returns all possible primary keys)       ##
    ##                                                                                  ##
    ######################################################################################
    \n
    """
    try:
        if isinstance(df, pd.DataFrame):
            if args == ():
                i = 0
                j = list()
                for i in range(0, len(list(df.columns))):
                    if df.iloc[:, i].nunique() == zsnobs(df):
                        j.append(df.columns[i])
                print('Following columns can be used as primary key : ' + str(j))

            else:

                df1 = df[df.duplicated(list(args), keep=False)]
                if zsnobs(df1) == 0:
                    if len(args) == 1:
                        print(str(args)[1:-2] + ' is a valid primary key for the data')
                    else:
                        print(str(args)[1:-1] + ' is a valid primary key for the data')
                else:
                    if len(args) == 1:
                        print(f'{zsnobs(df1)} Duplicates found at {str(args)[1:-2]} level')
                        return df1
                    else:
                        print(f'{zsnobs(df1)} Duplicates found at {str(args)[1:-1]} level')
                        return df1
        else:
            print("Invalid Argument - First Argument must be a pandas dataframe")
            raise AttributeError
    except AttributeError:
        print("Error Occured :  incorrect input type. This function only works for Dataframe")
        print(zsprikey.__doc__)
        return "Error"
    except KeyError:
        print('Error : argument/column(s) not present in dataframe')
        print(zsprikey.__doc__)
        return "Error"
    except Exception as e:
        print(e)
        print(zsprikey.__doc__)
        return "Error";


def zsutlcrun(df, index_list, column_list, value_list, func='sum', m=False, m_name='Total', fn='-'):
    """\n
    ######################################################################################
    ##                           USAGE OF ZSUTLCRUN FUNCTION                            ##
    ##                                                                                  ##
    ##   UTLCRUN is used to “pivot up” a “narrow” and “tall” dataset into one that      ##
    ##   is “wide” and “short”                                                          ##
    ##                                                                                  ##
    ##   Usage                                                                          ##
    ##   > from Macros import zsutlcrun                                                 ##
    ##   > zsutlcrun(df,index_list,column_list,value_list,func = sum, m = false,        ##
    ##     m_name = Total , fn = -)                                                     ##
    ##                                                                                  ##
    ##    **where                                                                       ##
    ##       --df is the dataframe,                                                     ##
    ##       --index_list is/are the column(s) to be made index (output dataset will    ##
    ##            be unique at this level)                                              ##
    ##       --column_list are the column(s) to pivot up                                ##
    ##       --value_list are the columns to be aggregated,                             ##
    ##       --func is the type of aggregation to be done (optional; by default sum;    ##
    ##            possible aggregations: sum, count, mean, median, max, min)            ##
    ##       --m is to toggle row and column level subtotals (optional; by              ##
    ##            default False)                                                        ##
    ##       --m_name is to name the subtotal columns (optional; by default "Total")    ##
    ##       --fn is to fill blanks in the pivoted table (optional; by default "-")     ##
    ##                                                                                  ##
    ######################################################################################
    \n
    """
    try:
        if isinstance(df, pd.DataFrame):
            if func != 'count':
                for j in range(len(value_list)):
                    if df[value_list[j]].dtype != 'O':
                        i = 2
                    else:
                        i = 0
                        return (f' For {func} agg function, all Value fields need to be numeric type')
                df1 = df.pivot_table(index=index_list, columns=column_list, values=value_list, aggfunc=func, margins=m,
                                     margins_name=m_name, fill_value=fn)
                df1.columns = [''.join(f'{col}').strip() for col in df1.columns]
                return df1.reset_index()
            else:
                df1 = df.pivot_table(index=index_list, columns=column_list, values=value_list, aggfunc=func, margins=m,
                                     margins_name=m_name, fill_value=fn)
                df1.columns = [''.join(f'{col}').strip() for col in df1.columns]
                return df1.reset_index()
        else:
            print("Invalid Argument - First Argument must be a pandas dataframe")
            raise AttributeError
    except AttributeError:
        print("Error Occured :  incorrect input type. This function only works for Dataframe")
        print(zsutlcrun.__doc__)
        return "Error"
    except KeyError:
        print('Error : argument/column(s) not present in dataframe')
        print(zsutlcrun.__doc__)
        return "Error"
    except Exception as e:
        print(e)
        print(zsutlcrun.__doc__)
        return "Error";


