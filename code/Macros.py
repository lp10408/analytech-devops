"""\n
########################################################################################################################
##                                         USER GUIDE - ZS MACRO                                                      ##
##   This is a python library with following reusable functions:                                                      ##
##   -  zsmean                                                                                                        ##
##                                                                                                                    ##
##   To know more about the function use help function like belos                                                     ##
##   Usage: help(<function name>)                                                                                     ##
##   > help(zsmean)                                                                                                   ##
##                                                                                                                    ##
########################################################################################################################
\n"""
import pandas as pd

class ParameterError(Exception):
    "Custom Errors"

def zsmeans(df):
	"""\n
    ####################################################################################################################
    ##                                      USAGE OF ZSMEAN FUNCTION                                                  ##
    ##                                                                                                                ##
    ##   This function performs ---------------------------------                                                     ##
    ##   Usage:                                                                                                       ##
    ##   > from Macros import zsmeans                                                                                 ##
    ##   > import pandas as pd                                                                                        ##
    ##   > d = {'col1': [1, 2], 'col2': [3, 4]}                                                                       ##
    ##   > df = pd.DataFrame(d)                                                                                       ##
    ##   > zsmeans(df)                                                                                                ##
    ##                                                                                                                ##
    ####################################################################################################################
    \n
    """
	try:
		if isinstance(df, pd.DataFrame):
			output = df.describe().unstack().unstack();
			return output
		else:
			print("Invalid Argument - First Argument must be a pandas dataframe")
			raise ParameterError
	except ParameterError:
		print("Error Occured")
		print(zsmeans.__doc__)
		raise ParameterError
	except Exception:
		print(zsmeans.__doc__)


