import os
import pandas as pd
import sys
import unittest
from pandas._testing import assert_frame_equal

code_path = os.path.normpath(os.path.dirname(os.path.realpath(__file__)) + os.sep + os.pardir) + "/code/"
sys.path.append(code_path)
print(code_path)

# from unnecessary_math import abc
from macros2 import *

data = {'Name': ['Tom', 'nick', 'krish', 'jack'], 'Age': [20, 21, 19, 18]}
df = pd.DataFrame(data)


class zsmeansTest(unittest.TestCase):
    def test_zsmeans_invalidparameter(self):
        assert zsmeans("dummy") == 'Error'

    def test_zsmeans_successrun(self):
        data = {'Name': ['Tom', 'nick', 'krish', 'jack'], 'Age': [20, 21, 19, 18]}
        df1 = pd.DataFrame(data).describe().unstack().unstack();
        assert_frame_equal(zsmeans(df), df1)


class zsmeans2Test(unittest.TestCase):
    def test_zsmeans2_invalid_input(self):
        df1 = 'Dummy'
        assert zsmeans2(df1, 'Name') == 'Error'

    def test_zsmeans2_invalid_args(self):
        assert zsmeans2(df, 'Class') == 'Error'

    def test_zsmeans2_successrun(self):
        data = {'Name': ['Tom', 'nick', 'krish', 'jack'], 'Age': [20, 21, 19, 18]}
        df1 = pd.DataFrame(data)[['Age']].describe().unstack().unstack();
        assert_frame_equal(zsmeans(df, 'Age'), df1)


class zsnobsTest(unittest.TestCase):
    def test_zsnobs_successrun(self):
        assert zsnobs(df) == df.shape[0]

    def test_zsnobs_invalidparameter(self):
        assert zsnobs("dummy") == 'Error'


class zssampTest(unittest.TestCase):

    def test_zssamp_invalid_input(self):
        assert zssamp("dummy") == 'Error'

    def test_zssamp_invalid_args(self):
        assert zssamp(df, 'Class') == 'Error'

    def test_zssamp_firstrecords(self):
        n = 3
        df1 = zssamp(df, n=n).loc[['First']].reset_index().iloc[:, 1:]
        df2 = df.head(n)
        assert len(zssamp(df, n=n).loc[['First']]) == len(df.head(n))
        assert_frame_equal(df1, df2)

    def test_zssamp_randomrecords(self):
        n = 3
        assert len(zssamp(df, n=n).loc[['Random']]) == len(df.sample(n))

    def test_zssamp_lastrecords(self):
        n = 3
        df1 = zssamp(df, n=n).loc[['Last']].reset_index().iloc[:, 1:]
        df2 = df.tail(n).reset_index().iloc[:, 1:]
        assert len(zssamp(df, n=n).loc[['Last']]) == len(df.tail(n))
        assert_frame_equal(df1, df2)

    def test_zssamp_firstrecords_w_arg(self):
        n = 3
        df1 = zssamp(df, 'Name', n=n).loc[['First']].reset_index().iloc[:, 1:]
        df2 = df.head(n).loc[:, ['Name']]
        assert len(zssamp(df, 'Name', n=n).loc[['First']]) == len(df2)
        assert_frame_equal(df1, df2)

    def test_zssamp_randomrecords_w_arg(self):
        n = 3
        assert len(zssamp(df, 'Name', n=n).loc[['Random']]) == len(df.sample(n))

    def test_zssamp_lastrecords_w_arg(self):
        n = 3
        df1 = zssamp(df, 'Name', n=n).loc[['Last']].reset_index().iloc[:, 1:]
        df2 = df.tail(n).reset_index().iloc[:, 1:].loc[:, ['Name']]
        assert len(zssamp(df, 'Name', n=n).loc[['Last']]) == len(df2)
        assert_frame_equal(df1, df2)


class zsutlcrunTest(unittest.TestCase):

    def test_zsutlcrun_invalid_input(self):
        df1 = "dummy"
        assert zsutlcrun(df1, ['Name'], ['Age'], ['Age']) == 'Error'

    def test_zsutlcrun_invalid_args(self):
        assert zsutlcrun(df, ['Name'], ['Age'], ['Class']) == 'Error'

    def test_zsutlcrun_sum_check(self):
        data1 = {'Name': ['A', 'A', 'B', 'B', 'B', 'C', 'C', 'C', 'D', 'D', 'D', 'D', 'E', 'E', 'E'],
                 'Class': ['X', 'X', 'X', 'X', 'X', 'Y', 'Y', 'Y', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z'],
                 'Marks': [1, 2, 3, 4, 5, 6, 7, 8, 9, 7, 6, 5, 4, 3, 2]}
        data2 = {'Name': ['A', 'B', 'C', 'D', 'E'],
                 'X': [3, 12, 0, 0, 0],
                 'Y': [0, 0, 21, 0, 0],
                 'Z': [0, 0, 0, 27, 9]}

        df1 = pd.DataFrame(data1)
        df1_sum = pd.DataFrame(data2)
        df1_sum.columns = range(df1_sum.shape[1])

        op1 = zsutlcrun(df1, ['Name'], ['Class'], ['Marks'], func='sum', fn=0)
        op1.columns = range(op1.shape[1])

        assert_frame_equal(op1, df1_sum)

    def test_zsutlcrun_count_check(self):
        data1 = {'Name': ['A', 'A', 'B', 'B', 'B', 'C', 'C', 'C', 'D', 'D', 'D', 'D', 'E', 'E', 'E'],
                 'Class': ['X', 'X', 'X', 'X', 'X', 'Y', 'Y', 'Y', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z'],
                 'Marks': [1, 2, 3, 4, 5, 6, 7, 8, 9, 7, 6, 5, 4, 3, 2]}

        data3 = {'Name': ['A', 'B', 'C', 'D', 'E'],
                 'X': [2, 3, 0, 0, 0],
                 'Y': [0, 0, 3, 0, 0],
                 'Z': [0, 0, 0, 4, 3]}
        df1 = pd.DataFrame(data1)

        df1_count = pd.DataFrame(data3)
        df1_count.columns = range(df1_count.shape[1])

        op2 = zsutlcrun(df1, ['Name'], ['Class'], ['Marks'], func='count', fn=0)
        op2.columns = range(op2.shape[1])

        assert_frame_equal(op2, df1_count)
        # assert_frame_equal(zsutlcrun(df1,['Name'],['Class'],['Marks'],func = 'count',fn = '0'),df1_count)


class zsncolTest(unittest.TestCase):
    def test_zsncol_successrun(self):
        assert zsncol(df) == df.shape[1]

    def test_zscol_invalidparameter(self):
        assert zsncol("dummy") == 'Error'


inp = {'Name': ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J'],
       'Class': ['X', 'X', 'X', 'Y', 'Y', 'Y', 'Z', 'Z', 'Z', 'Z'],
       'Marks': [1, 2, 3, 4, 5, 6, 7, 8, 9, 5]}
inp = pd.DataFrame(inp)

out = {'Name': ['A', 'B', 'C', 'D', 'E', 'J', 'F', 'G', 'H', 'I'],
       'Class': ['X', 'X', 'X', 'Y', 'Y', 'Z', 'Y', 'Z', 'Z', 'Z'],
       'Marks': [1, 2, 3, 4, 5, 5, 6, 7, 8, 9]}

out = pd.DataFrame(out)


class zssortTest(unittest.TestCase):

    def test_zssort_invalid_input(self):
        df1 = "dummy"
        assert zssort(df1, 'Class') == 'Error'

    def test_zssort_invalid_args(self):
        assert zssort(inp, 'Name', 'Age', 'Class') == 'Error'

    def test_zssort_successrun(self):
        assert_frame_equal(zssort(inp, 'Marks').reset_index().iloc[:, 1:], out)


class zssort2Test(unittest.TestCase):

    def test_zssort2_invalid_input(self):
        df1 = "dummy"
        assert zssort2(df1, 'Class') == 'Error'

    def test_zssort2_invalid_args(self):
        assert zssort2(inp, 'Name', 'Age', 'Class') == 'Error'

    def test_zssort2_successrun(self):
        assert_frame_equal(zssort2(inp, 'Marks').reset_index().loc[:, ['Marks']], out.loc[:, ['Marks']])


class zsrandomTest(unittest.TestCase):

    def test_zsrandom_invalid_input(self):
        assert zsrandom("dummy") == 'Error'

    def test_zsrandom_invalid_args(self):
        assert zsrandom(df, 'Class') == 'Error'

    def test_zsrandom_successrun(self):
        n = 3
        assert len(zsrandom(df, n=n)) == len(df.sample(n))

    def test_zsrandom_successrun_w_arg(self):
        n = 3
        assert len(zsrandom(df, 'Name', n=n)) == len(df.sample(n))


class zsrankbyTest(unittest.TestCase):

    def test_zsrankby_invalid_input(self):
        df1 = "dummy"
        assert zsrankby(df1, 'Class', 5, x=0) == 'Error'

    def test_zsrankby_invalid_args(self):
        assert zsrankby(inp, 'Age', 5, x=0) == 'Error'

    def test_zsrankby_successrun_x0(self):
        op = pd.DataFrame({'Name': ['I', 'H', 'G', 'F', 'E', 'J', 'D', 'C', 'B', 'A'],
                           'Class': ['Z', 'Z', 'Z', 'Y', 'Y', 'Z', 'Y', 'X', 'X', 'X'],
                           'Marks': [9, 8, 7, 6, 5, 5, 4, 3, 2, 1],
                           '__Bucket__': [1, 1, 2, 2, 3, 3, 4, 4, 5, 5]})
        assert_frame_equal(zsrankby(inp, 'Marks', 5, x=0).reset_index().iloc[:, 1:], op)

    def test_zsrankby_successrun_x1(self):
        op = pd.DataFrame({'Name': ['I', 'H', 'G', 'F', 'E', 'J', 'D', 'C', 'B', 'A'],
                           'Class': ['Z', 'Z', 'Z', 'Y', 'Y', 'Z', 'Y', 'X', 'X', 'X'],
                           'Marks': [9, 8, 7, 6, 5, 5, 4, 3, 2, 1],
                           '__Bucket__': [1, 2, 3, 3, 4, 4, 5, 5, 5, 5]})
        assert_frame_equal(zsrankby(inp, 'Marks', 5, x=1).reset_index().iloc[:, 1:], op)

    def test_zsrankby_successrun_x2(self):
        op = pd.DataFrame({'Name': ['A', 'B', 'C', 'D', 'E', 'J', 'F', 'G', 'H', 'I'],
                           'Class': ['X', 'X', 'X', 'Y', 'Y', 'Z', 'Y', 'Z', 'Z', 'Z'],
                           'Marks': [1, 2, 3, 4, 5, 5, 6, 7, 8, 9],
                           '__Bucket__': [5, 5, 4, 4, 3, 3, 2, 2, 1, 1]})
        assert_frame_equal(zsrankby(inp, 'Marks', 5, x=2).reset_index().iloc[:, 1:], op)

    def test_zsrankby_successrun_w_arg_x0(self):
        op = pd.DataFrame({'Name': ['I', 'H', 'G', 'J', 'F', 'E', 'D', 'C', 'B', 'A'],
                           'Class': ['Z', 'Z', 'Z', 'Z', 'Y', 'Y', 'Y', 'X', 'X', 'X'],
                           'Marks': [9, 8, 7, 5, 6, 5, 4, 3, 2, 1],
                           '__Bucket__': [1, 2, 3, 3, 1, 2, 3, 1, 2, 3]})
        assert_frame_equal(zsrankby(inp, 'Marks', 3, 'Class', x=0).reset_index().iloc[:, 1:], op)

    def test_zsrankby_successrun_w_arg_x1(self):
        op = pd.DataFrame({'Name': ['I', 'H', 'G', 'J', 'F', 'E', 'D', 'C', 'B', 'A'],
                           'Class': ['Z', 'Z', 'Z', 'Z', 'Y', 'Y', 'Y', 'X', 'X', 'X'],
                           'Marks': [9, 8, 7, 5, 6, 5, 4, 3, 2, 1],
                           '__Bucket__': [1, 2, 3, 3, 2, 3, 3, 2, 3, 3]})
        assert_frame_equal(zsrankby(inp, 'Marks', 3, 'Class', x=1).reset_index().iloc[:, 1:], op)

    def test_zsrankby_successrun_w_arg_x2(self):
        op = pd.DataFrame({'Name': ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J'],
                           'Class': ['X', 'X', 'X', 'Y', 'Y', 'Y', 'Z', 'Z', 'Z', 'Z'],
                           'Marks': [1, 2, 3, 4, 5, 6, 7, 8, 9, 5],
                           '__Bucket__': [3, 2, 1, 3, 2, 1, 3, 2, 1, 3]})
        assert_frame_equal(zsrankby(inp, 'Marks', 3, 'Class', x=2).reset_index().iloc[:, 1:], op)


if __name__ == '__main__':
    unittest.main()
