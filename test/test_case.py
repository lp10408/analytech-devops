import os
import sys
import pandas as pd

code_path = os.path.normpath(os.path.dirname(os.path.realpath(__file__)) + os.sep + os.pardir) + "/code/"
sys.path.append(code_path)
print(code_path)

from unnecessary_math import abc
from Macros import *

def test_numbers_3_4():
    assert abc().multiply(3,4) == 12

def test_strings_a_3():
    assert abc().multiply('a',3) == 'aaa'

def test_strings_5_3():
    assert abc().multiply(5,3) == 15

def test_strings_5_4():
    assert abc().multiply(5,4) == 20

def test_strings_5_2():
    assert abc().multiply(5,2) == 10
