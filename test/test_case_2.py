import os
import sys
import pandas as pd
import unittest
from pandas._testing import assert_frame_equal
code_path = os.path.normpath(os.path.dirname(os.path.realpath(__file__)) + os.sep + os.pardir) + "/code/"
sys.path.append(code_path)
print(code_path)

from unnecessary_math import abc
from Macros import *

class zsmeansTest(unittest.TestCase):
    def test_zsmeans_invalidparameter(self):
        self.assertRaises(ParameterError, lambda:zsmeans("dummy param"))

    def test_zsmean_successrun(self):
        data = {'Name':['Tom', 'nick', 'krish', 'jack'], 'Age':[20, 21, 19, 18]}
        df = pd.DataFrame(data).describe().unstack().unstack();
        df2 = pd.DataFrame(data)
        assert_frame_equal(zsmeans(df2), df)

if __name__=='__main__':
    unittest.main()
